import { Component, Input } from '@angular/core';
import template from './title.pug';
import { UserService } from '../services/user.service';


@Component( {
  selector: 'app-title',
  template,
} )

export class TitleComponent {
  @Input() subtitle = '';
  title = 'Angular Modules';
  user: string = '';

  constructor( userService: UserService ) {
    this.user = userService.userName;
  }
}

