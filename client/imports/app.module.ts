import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { trace, Category, UIRouterModule, UIView } from "@uirouter/angular";
import { AppComponent } from './app.component';
import { MAIN_STATES } from "./app.states";
import { HighlightDirective } from './common/highlight/highlight.directive';
import { TitleComponent } from './common/title/title.component';
import { UserService } from './common/services/user.service';



@NgModule( {
  imports: [
    BrowserModule,
    UIRouterModule.forRoot( {
      states: MAIN_STATES,
      otherwise: { state: 'app', params: {} },
      useHash: true,
    } ),
  ],
  declarations: [
    AppComponent,
    HighlightDirective,
    TitleComponent,
  ],
  bootstrap: [
    UIView
    // AppComponent
  ],
  providers:
  [
    UserService,
  ]

} )

export class AppModule {


}